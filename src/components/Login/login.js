import React from 'react'
import './style.css'
import { Redirect } from 'react-router-dom'
import cookie from 'react-cookies'

export default class Login extends React.Component {
    constructor(){
        super();

        this.state = {
            user: '',
            pass: '',
            redirect: false
        };
        
        this.handleChangePass = this.handleChangePass.bind(this);
        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.authorization = this.authorization.bind(this);
    }

    handleChangePass(event){
        this.setState({
            pass: event.target.value
        })
    }
    
    handleChangeUser(event){
        this.setState({
            user: event.target.value
        })
    }
    
    authorization(event){
        event.preventDefault();
        const body = {
            username: this.state.user,
            password: this.state.pass,
        };
        if(this.state.user.length === 11){
            cookie.save("userId", this.state.user);
            this.setState({
                redirect: true
            })
        }
    };
    
    render() {
        if(this.state.redirect){
            return <Redirect to="/"></Redirect>
        }
        var submitBut = <button type="submit" className="btn btn-primary">ورود</button>
        return(
            <div className="container bg-light p-3">
                <form className="login-form" onSubmit={this.authorization}>
                    <div className="form-row ">
                        <div className="col-sm-6 form-group">
                            <input value={this.state.user} onChange={this.handleChangeUser} type="text" className="form-control" placeholder="شماره موبایل" />
                        </div>
                    </div>
                    <div className="form-row ">
                        <div className="col-sm-6 form-group">
                            <input value={this.state.pass} onChange={this.handleChangePass} type="password" className="form-control" placeholder="رمز عبور"/>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary">ورود</button>
                </form>
            </div>
        );
    }
}