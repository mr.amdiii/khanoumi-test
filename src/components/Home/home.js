import React from 'react'
import ReactDOM from 'react-dom'
import './style.css'
import InfiniteScroll from 'react-infinite-scroller'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import cookie from 'react-cookies'


function ProductCard(props){
	let user_id = cookie.load("userId");
	let favs = cookie.load(user_id)
	let isButton = true;
	console.log(favs[0], props.product.id)
	if(favs !== undefined){
		for(let i=0; i < favs.length; i++){
			if(favs[i] === props.product.id){
				isButton = false;
				break;
			}
		}
	}

	const addToFav = (e) => {
		console.log("here")
		favs.push(props.product.id);
		cookie.save(user_id, favs)
		document.getElementById("myid").style.display = "none";	
	}
    return(
		<div className="product-card shadow">
			<img alt='' className="home-product-img border" src={props.product.img}/>
			<div>
				<div className="product-firstrow">
					<div className="home-product-title">
						{props.product.title}
					</div>
				</div>
				<div className="product-budget">قیمت: {props.product.price}</div>
				<div className="descriptions">
					<div id='desc' className="description">توضیحات: {props.product.desc}</div>
					<div id='full_desc' className="full-description">توضیحات: {props.product.description}</div>
				</div>
				{isButton ?
					<button id="myid" className="btn btn-primary" onClick={addToFav} >اضافه به علاقه مندی ها </button>
					:
					<div>این محصول در لیست علاقه مندی شما قرار دارد</div>
				}
			</div>
		</div>
	)
}


export default class Home extends React.Component{
    constructor(props){
		super(props);
        this.state = {
            product: [
                {
                  "id": 1,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 2,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 3,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 4,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 5,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 6,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                },
                {
                  "id": 7,
                  "title": "کرم دست",
                  "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
                  "desc": "این محصول",
                  "description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
                  "price": "۱۵۰۰۰"
                }
              ],
            redirect: cookie.load("userId") === undefined ? true : false
        }
	}
	
	loadItems(){
		var items = this.state.product;
		var data = {"id": 1,
			"title": "کرم دسmfkdsfkdsoت",
			"img": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVSDHhz_SPPqEDzwtSPurUpQwq_F70lchGZpiboVFld4Wrf4wb",
			"desc": "این محصول",
			"description": "این محصول دارای بته ایعسه بیسهینتمدیهعبعبایتذ بذساهاسهبذسعباهب",
			"price": "۱۵۰۰۰"
		}
		var config = {
            headers: {"Authorization": localStorage.getItem("Joboonja-token")}
        };
		axios.get("https://cors-anywhere.herokuapp.com/https://www.google.com/")
			.then(res => {
				items.push(data);
				items.push(data);
				items.push(data);
				items.push(data);
				items.push(data);
				items.push(data);
				this.setState({
					product: items
				})
			});
	}

    render(){
		var items = [];
		this.state.product.map((product, i) => {
            items.push(
				<ProductCard product={product}></ProductCard>
            );
		});
		if(this.state.redirect){
            return <Redirect to="/login"></Redirect>
        }
        return (	
            <div>
				<img className="full-width margin-bot" alt="" src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/53e2498c-1427-4c4d-a7d1-d4c8c0dfba79/d6l7qk9-2fe0c532-21ed-4607-b197-f22c9492c828.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzUzZTI0OThjLTE0MjctNGM0ZC1hN2QxLWQ0YzhjMGRmYmE3OVwvZDZsN3FrOS0yZmUwYzUzMi0yMWVkLTQ2MDctYjE5Ny1mMjJjOTQ5MmM4MjgucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.0DmRMx4EqZMZSFSCg4CzcfqzV_-uRVxQJE2BaGM2etI"></img>
				<InfiniteScroll
					pageStart={0}
					loadMore={this.loadItems.bind(this)}
					hasMore={true}
					loader={<div className="loader" key={0}>Loading ...</div>}
				>
					{items}
				</InfiniteScroll>
            </div>
        );
    }
} 