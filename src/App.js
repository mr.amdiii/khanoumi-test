import React from 'react';
import './App.css';
import Home from './components/Home/home'
import {BrowserRouter as Router, Route} from "react-router-dom";
import Login from './components/Login/login';

function App() {
    return (
		<div className="App">
			<Router>
				<Route exact path="/" component={Home}/>
				<Route path="/login" component={Login}></Route>
			</Router>
		</div>
    );
}

export default App;
